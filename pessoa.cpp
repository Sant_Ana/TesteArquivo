#include <iostream>
#include <string>

using namespace std;

class Pessoa {
private:
        string  nome;
        string  idade;
        string  telefone;
public:
        Pessoa(){}
        Pessoa(string nome, string idade, string telefone){}
        string  getIdade()
        {
            return idade;
        }
        void setIdade(string idade)
        {
            this->idade = idade;
        }
        string getNome()
        {
            return nome;
        }
        void setNome(string nome)
        {
            this->nome = nome;
        }
        string getTelefone()
        {
            return telefone;
        }
        void setTelefone(string telefone)
        {
            this->telefone = telefone;
        }
};

int main()
{
    Pessoa potter;
    potter.setNome("Potter Gay");
    potter.setIdade("12");
    potter.setTelefone("99084700");
    cout << potter.getNome();
    return 0;
}
