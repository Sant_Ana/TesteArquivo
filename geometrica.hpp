#ifndef GEOMETRICA_H
#define GEOMETRICA_H


class geometrica{
    private:
        float base,altura;
    public:
        geometrica();
        geometrica(float base,float altura);
        void setBase(float base);
        void setAltura(float altura);
        float getAltura();
        float getBase();
        float area();
}
#endif
