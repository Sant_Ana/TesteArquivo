#include "aluno.hpp"
#include <string>
#include <stdio.h>

using namespace std;
int num_materia = 0;

Aluno::Aluno(){
	matricula = 0;
	ira = 0.0;
}
Aluno::Aluno(string m1,string m2,string m3,string m4,string m5,string m6,string nome_in,string idade_in,string telefone_in,double ira_in,int matricula_in)
{
	setNome(nome_in);
	setTelefone(telefone_in);
	setIdade(idade_in);
	ira = ira_in;
	setMatricula(matricula_in);
	materia[0] = m1;
	materia[1] = m2;
	materia[2] = m3;
	materia[3] = m4;
	materia[4] = m5;
	materia[5] = m6;

}
//Métodos de Aluno
void Aluno::setMatricula(int matricula_in)
{
	
	matricula = matricula_in;
}
int Aluno::getMatricula()
{
	return matricula;
}
void Aluno::setMateria()
{
	
	int escolha,loop = 1; // Esta variavel será usada para toda entrada do tipo inteira do usuário nesse método
	while(loop)
	{
			printf("O que deseja fazer:\n1--Adicionar materia\n2--Substituir materia\n3--Sair\n");
	cin >> escolha;
	switch(escolha)
	{
		case 1:
		{
			while(num_materia <= 5)
			{
				printf("Insira a materia desejada:\n");
				cin >> materia[num_materia];
				num_materia++;
				break;
			}
			if(num_materia >= 6 )
			cout <<"O aluno ja possui 6 materias\n";
		}	break;

		case 2:
		{
			int contagem_materias = 0 ;
			printf("Qual matéria deseja substituir:\n");
			while(contagem_materias < 6) //Imprime as matérias na tela
			{
				cout << contagem_materias+1 << " -- " << materia[contagem_materias ] << "\n" <<endl;
				contagem_materias++;
			}

			scanf("%i",&escolha);
			printf("Nova materia:\n");
			cin >> materia[escolha-1];
			contagem_materias = 0;
			cout << "Novas Materias\n";
			while(contagem_materias < 6) //Imprime as matérias na tela
			{
				cout << contagem_materias+1 << " -- " << materia[contagem_materias ] << "\n" <<endl;
				contagem_materias++;
			}


		}	break;
		case 3 : loop =0;
		break;
		default: 
			printf("Escolha Invalida.\n");
	}
	}
	
}
void Aluno::getMateria()
{
	num_materia = 0;
	cout << "MATERIAS\n";
	 while(num_materia < 6) //Imprime as matérias na tela
	 {

	 	cout << num_materia+1 << " -- " << materia[num_materia] << "\n" <<endl;
	 	num_materia++;
	 }
				
}

void Aluno::setIra(double ira_in)
{
	ira = ira_in;
	
}
double Aluno::getIra()
{
	return ira;
}